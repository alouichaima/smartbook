import { Router } from '@angular/router';
import { LibraireService } from './../services/libraire.service';
import { libraire } from './../model/libraire';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-libraire',
  templateUrl: './add-libraire.component.html',
  styleUrls: ['./add-libraire.component.scss']
})
export class AddLibraireComponent implements OnInit {

    l: libraire=new libraire;
    constructor(private LibraireService: LibraireService,
                 private router: Router) {
     }
     ngOnInit(): void {

     }
     AddLibraire(): void
     {
       this.LibraireService.addLibraire(this.l)
         .subscribe(data=>
         {
           console.log(data);
         }, error => console.log(error));
     }


     toLibraire(): void
     {
         this.router.navigate(['/LibrairesList']);
     }



     onSubmit():void
 {
 this.LibraireService.addLibraire(this.l).subscribe(
 (data)=>console.log(data),

 (error)=>console.log(error),
 ()=>this.toLibraire()

 );
 console.log(this.l);

 }

     retour(): void {
       this.router.navigate(['/LibrairesList']);
     }


}
