import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListelibraireComponent } from './listelibraire.component';

describe('ListelibraireComponent', () => {
  let component: ListelibraireComponent;
  let fixture: ComponentFixture<ListelibraireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListelibraireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListelibraireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
