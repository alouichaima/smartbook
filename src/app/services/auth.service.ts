import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Utilisateur } from '../model/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /*utilisateurs: Utilisateur[] = [{"email":"admin@gmail.com","password":"100"},
                                 {"email":"oumaimaguedri@gmail.com","password":"123"} ];*/
                            
  public loggedUtilisateur: string | undefined;
  
  public isloggedIn: Boolean = false;


  apiURL: string = 'http://localhost:8484/SmartBookStoreREST/rest/users/login';

  getUtilisateurFromDB(email:string , password:string):Observable<Utilisateur>
{
const url = `${this.apiURL}/${email}/${password}`;
return this.http.get<Utilisateur>(url);
}
                                 
  constructor(private router: Router, private http : HttpClient) { }
  
  logout() {
    this.isloggedIn= false;
    this.loggedUtilisateur = undefined;

    localStorage.removeItem('loggedUtilisateur');
    localStorage.setItem('isloggedIn',String(this.isloggedIn));
    this.router.navigate(['/home']);
    }
    

  signIn(utilisateur :Utilisateur){
    this.loggedUtilisateur = utilisateur.email;
    this.loggedUtilisateur = utilisateur.password;
    this.isloggedIn = true;
    localStorage.setItem('loggedUtilisateur',this.loggedUtilisateur!);
    localStorage.setItem('isloggedIn',String(this.isloggedIn));
    }

    setLoggedUserFromLocalStorage(loginf : string) {
      this.loggedUtilisateur = loginf;
      this.isloggedIn = true;
      }
    
    
}
