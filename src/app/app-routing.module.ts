import { EquipeComponent } from './equipe/equipe.component';
import { EditlivreComponent } from './editlivre/editlivre.component';
import { AddLivreComponent } from './add-livre/add-livre.component';
import { LivresComponent } from './livres/livres.component';
import { GererCompteComponent } from './gerer-compte/gerer-compte.component';
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ScienceComponent } from './science/science.component';
import { InforinternetComponent } from './inforinternet/inforinternet.component';
import { LitteratureComponent } from './litterature/litterature.component';
import { DevperComponent } from './devper/devper.component';
import { CategorieComponent } from './categorie/categorie.component';
import { ContactComponent } from './contact/contact.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { TableComponent } from './page/table/table.component';
import { ListelibraireComponent } from './listelibraire/listelibraire.component';
import { ListelecteurComponent } from './listelecteur/listelecteur.component';
import { LecteursComponent } from './lecteurs/lecteurs.component';
import { AddLecteurComponent } from './add-lecteur/add-lecteur.component';
import { EditlecteurComponent } from './editlecteur/editlecteur.component';
import { AnnonceComponent } from './annonce/annonce.component';
import { PanierComponent } from './panier/panier.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'table', component: TableComponent},
  { path: '', redirectTo: '', component:LoginComponent},
  {path:'contact', component:ContactComponent},
  {path:'categorie', component:CategorieComponent},
  {path:'devper', component:DevperComponent},
  {path:'litterature', component:LitteratureComponent},
  {path:'infointernet', component:InforinternetComponent},
  {path:'science', component:ScienceComponent},
  {path:'inscription', component:InscriptionComponent},
  {path:'login', component:LoginComponent},
  {path:'gerer-compte', component:GererCompteComponent},
  {path:'livres', component:LivresComponent},
  {path:'addlivre', component:AddLivreComponent},
  {path:'LivresList', component:LivresComponent},
  {path:'editlivres', component:EditlivreComponent},
  {path:'update/:numSerie' , component:EditlivreComponent},
  {path: 'livresList', component:AddLivreComponent},
  {path:'lecteurs', component:LecteursComponent},
  {path:'addlecteur', component:AddLecteurComponent},
  {path:'editlecteur', component:EditlecteurComponent},
  {path:'update/:cin' , component:EditlecteurComponent},
  {path: 'lecteurs', component:AddLecteurComponent},
  {path:'listelibraire', component:ListelibraireComponent},
  {path:'listelecteur', component:ListelecteurComponent},
  {path: 'annonce', component:AnnonceComponent},
  {path: 'panier', component:PanierComponent},
  {path: 'equipe', component:EquipeComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
