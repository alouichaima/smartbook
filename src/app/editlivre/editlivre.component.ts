import { LivreService } from './../services/livre.service';
import { Livre } from './../model/livre';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-editlivre',
  templateUrl: './editlivre.component.html',
  styleUrls: ['./editlivre.component.scss']
})
export class EditlivreComponent implements OnInit {
    l: Livre = new Livre();
    numSerie:any;


      constructor(private service :LivreService,
                  private router: Router,
                  private route: ActivatedRoute) {
      }

      ngOnInit(): void {
        this.numSerie = this.route.snapshot.params. numSerie;
        this.service.getLivreBynumSerie(this.numSerie)
          .subscribe(  data => {
            console.log(data);
            this.l = data;
          }, error => console.log(error));
      }

      retour(): void {
        this.router.navigate(['/livres']);
      }
      toLivre(): void
      {
        this.router.navigate(['/livres']);

      }
      onSubmit(): void {
        this.service.updateLivre(this.l)
          .subscribe((data: any) =>
          console.log(data), (error: any) => console.log(error));
        this.toLivre();
        this.service.getAllLivre();
      }

}
