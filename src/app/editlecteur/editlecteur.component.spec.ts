import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditlecteurComponent } from './editlecteur.component';

describe('EditlecteurComponent', () => {
  let component: EditlecteurComponent;
  let fixture: ComponentFixture<EditlecteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditlecteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditlecteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
