import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditlibraireComponent } from './editlibraire.component';

describe('EditlibraireComponent', () => {
  let component: EditlibraireComponent;
  let fixture: ComponentFixture<EditlibraireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditlibraireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditlibraireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
