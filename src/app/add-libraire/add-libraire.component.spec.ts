import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLibraireComponent } from './add-libraire.component';

describe('AddLibraireComponent', () => {
  let component: AddLibraireComponent;
  let fixture: ComponentFixture<AddLibraireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddLibraireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLibraireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
