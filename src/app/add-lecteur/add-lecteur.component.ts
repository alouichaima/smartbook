import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Lecteurs } from '../model/lecteurs';
import { LecteurService } from '../services/lecteur.service';

@Component({
  selector: 'app-add-lecteur',
  templateUrl: './add-lecteur.component.html',
  styleUrls: ['./add-lecteur.component.scss']
})
export class AddLecteurComponent implements OnInit {

  l: Lecteurs = new Lecteurs();
  constructor(private lecteurService: LecteurService,
               private router: Router) {
   }



  ngOnInit(): void {
  }
  addlivre(): void
    {
      this.lecteurService.addLecteur(this.l)
        .subscribe(data=>
        {
          console.log(data);
        }, error => console.log(error));
    }


    toLecteurs(): void
    {
        this.router.navigate(['lecteurs']);
    }



    onSubmit():void
{
this.lecteurService.addLecteur(this.l).subscribe(
(data)=>console.log(data),

(error)=>console.log(error),
()=>this.toLecteurs()

);
console.log(this.l);

}

    retour(): void {
      this.router.navigate(['lecteurs']);
    }


 }


