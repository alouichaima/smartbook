import { Router, ActivatedRoute } from '@angular/router';
import { LibraireService } from './../services/libraire.service';
import { libraire } from './../model/libraire';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editlibraire',
  templateUrl: './editlibraire.component.html',
  styleUrls: ['./editlibraire.component.scss']
})
export class EditlibraireComponent implements OnInit {

    l: libraire = new libraire();
    cin:any;


      constructor(private service :LibraireService,
                  private router: Router,
                  private route: ActivatedRoute) {
      }

      ngOnInit(): void {
        this.cin = this.route.snapshot.params. numSerie;
        this.service.getLibraireBycin(this.cin)
          .subscribe(  data => {
            console.log(data);
            this.l = data;
          }, error => console.log(error));
      }

      retour(): void {
        this.router.navigate(['/libraires']);
      }
      toLivre(): void
      {
        this.router.navigate(['/libraires']);

      }
      onSubmit(): void {
        this.service.updateLibraire(this.l)
          .subscribe((data: any) =>
          console.log(data), (error: any) => console.log(error));
        this.toLivre();
        this.service.getAllLibraire();
      }

}
