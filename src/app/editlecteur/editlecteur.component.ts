import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Lecteurs } from '../model/lecteurs';
import { LecteurService } from '../services/lecteur.service';

@Component({
  selector: 'app-editlecteur',
  templateUrl: './editlecteur.component.html',
  styleUrls: ['./editlecteur.component.scss']
})
export class EditlecteurComponent implements OnInit {
  l: Lecteurs = new Lecteurs();
  cin:any;


      constructor(private service :LecteurService,
                  private router: Router,
                  private route: ActivatedRoute) {
      }



  ngOnInit(): void {
    this.cin = this.route.snapshot.params.cin;
    this.service.getLecteurBycin(this.cin)
      .subscribe(  data => {
        console.log(data);
        this.l = data;
      }, error => console.log(error));
  }
  retour(): void {
    this.router.navigate(['/lecteurs']);
  }
  toLecteurs(): void
  {
    this.router.navigate(['/lecteurs']);

  }
  onSubmit(): void {
    this.service.updateLecteur(this.l)
      .subscribe((data: any) =>
      console.log(data), (error: any) => console.log(error));
    this.toLecteurs();
    this.service.getAllLecteur();
  }

}

