import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lecteurs } from '../model/lecteurs';
import { LecteurService } from '../services/lecteur.service';

@Component({
  selector: 'app-lecteurs',
  templateUrl: './lecteurs.component.html',
  styleUrls: ['./lecteurs.component.scss']
})
export class LecteursComponent implements OnInit {

  Lecteur: Lecteurs [] = new Array;
  constructor(private lecteurService : LecteurService ,
      private router :Router) {

       }


ngOnInit(): void {
  this.reloadData();

}
reloadData():void{

   this.lecteurService.getAllLecteur().subscribe (data=>{
      this.Lecteur=data as Lecteurs [];
  });
}


deleteLecteur(cin: any): void
{
  this.lecteurService.supprimerLecteur(cin)
  .subscribe(data =>
    {
        console.log(data);
        this.reloadData();
    },
      error=>console.log(error));
    }
ajouter():void{
  this.router.navigate(['/addlecteur']);
}
toUpdate(cin: any){
  this.router.navigate(['update/:cin']);
}

}
