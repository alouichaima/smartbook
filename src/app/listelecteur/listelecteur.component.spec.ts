import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListelecteurComponent } from './listelecteur.component';

describe('ListelecteurComponent', () => {
  let component: ListelecteurComponent;
  let fixture: ComponentFixture<ListelecteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListelecteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListelecteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
